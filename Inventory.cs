﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FinalsProject
{
    public class InventoryList
    {
        [System.Xml.Serialization.XmlArray("Inventory"), System.Xml.Serialization.XmlArrayItem(typeof(Inventory), ElementName = "Item")]
        public List<Inventory> inventory { get; set; }

        public InventoryList()
        {
            inventory = new List<Inventory>();
        }
    }

    [Serializable()]
    [System.Xml.Serialization.XmlRoot("InventoryList")]
    public class Inventory
    {
        private string code, name;
        private double price;
        private int stock;

        public Inventory()
        {
        }

        public Inventory(string code, string name, double price, int stock)
        {
            this.code = code;
            this.name = name;
            this.price = price;
            this.stock = stock;
        }

        [System.Xml.Serialization.XmlAttribute("Code")]
        public string Code
        {
            get { return code; }
            set { this.code = value; }
        }

        [System.Xml.Serialization.XmlAttribute("Name")]
        public string Name
        {
            get { return name; }
            set { this.name = value; }
        }

        [System.Xml.Serialization.XmlAttribute("Price")]
        public double Price
        {
            get { return price; }
            set { this.price = value; }
        }

        [System.Xml.Serialization.XmlAttribute("Stock")]
        public int Stock
        {
            get { return stock; }
            set { this.stock = value; }
        }

        [System.Xml.Serialization.XmlAttribute("SalesTotal")]
        public double SalesTotal
        {
            get { return this.price * this.stock; }
        }
    }
}
