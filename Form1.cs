﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;

namespace FinalsProject
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            LoadState();
        }

        private void SaveState()
        {
            try
            {
                FileStream fs = new FileStream(@"file.txt", FileMode.Create);

                using (var writer = new StreamWriter(fs))
                {
                    // Loop through all rows and columns
                    for (int row = 0; row <= dataGridView1.RowCount; row++)
                    {
                        string lines = "";
                        for (int col = 0; col < 5; col++)
                        {
                            if (col == 4)
                            {
                                lines += dataGridView1.Rows[row].Cells[col].Value.ToString();
                            }
                            else
                            {
                                lines += dataGridView1.Rows[row].Cells[col].Value.ToString() + ",";
                            }
                        }
                        // Write format string to file
                        writer.WriteLine(lines);
                    }
                }
                fs.Close();
            }

            catch (Exception e) { }
        }

        private void LoadState()
        {
            try
            {
                FileStream fs = new FileStream(@"file.txt", FileMode.Open);

                using (StreamReader readin = new StreamReader(fs))
                {
                    int row = 0;
                    string line;
                    while ((line = readin.ReadLine()) != null)
                    //while (!readin.EndOfStream)
                    {
                        string[] columns = line.Split(',');
                        dataGridView1.Rows.Add();
                        for (int i = 0; i < columns.Length; i++)
                        {
                            dataGridView1[i, row].Value = columns[i];
                        }
                        row++;
                    }
                }

                //readin.Close();
                fs.Close();
            }
            catch (Exception e) { }
        }

        //public Inventory Search(string code)
        //{
        //    Inventory target = null;
        //    Add add = new Add();
        //    for (int row = 0; row < dataGridView1.RowCount; row++)
        //    {
        //        if (code == dataGridView1.Rows[row].Cells[0].Value.ToString())
        //        {
        //            MessageBox.Show(this, "Invalid code. Product already exists.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
        //            add.ShowDialog();
        //        }
        //    }
        //    return target;
        //}

        public List<string> CodeList()
        {
            var codes = new List<string>();
            for (int row = 0; row <= dataGridView1.RowCount; row++)
            {
                codes.Add(dataGridView1.Rows[row].Cells[0].Value.ToString());
            }
            return codes;
        }

        //public string Search()
        //{
        //    string[] codeArray = new string[30];
        //    string codes = null;

        //    for (int row = 0; row <= dataGridView1.RowCount; row++)
        //    {
        //        codeArray[row] = (dataGridView1.Rows[row].Cells[0].Value.ToString());
        //    }
        //    return codes;
        //}

        public void Add(List<Inventory> inventoryList)
        {
            foreach (Inventory inv in inventoryList)
            {
                dataGridView1.Rows.Add(inv.Code, inv.Name, String.Format("{0:N2}", inv.Price), inv.Stock, String.Format("{0:N2}", inv.SalesTotal));
            }
        }

        public List<Inventory> createEntry(string code, string name, double price, int stock)
        {
            List<Inventory> inventoryList = new List<Inventory>();

            // Entries
            Inventory entry = new Inventory(code, name, price, stock);
            inventoryList.Add(entry);

            return inventoryList;
        }

        private void btnAdd_Click(object sender, EventArgs e)
        {
            Add add = new Add();

            Hide();
            DialogResult answer = add.ShowDialog();
            Show();

            if (answer == DialogResult.OK)
            {
                Add(createEntry(add.Code, add.InvName, add.Price, add.Stock));
                SaveState();
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            Edit edit = new Edit();
            Hide();
            edit.Text = "Edit Item " + dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
            edit.InvName = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
            edit.Price = double.Parse(dataGridView1.SelectedRows[0].Cells[2].Value.ToString());
            DialogResult answer = edit.ShowDialog();
            Show();

            if (answer == DialogResult.OK)
            {
                dataGridView1.SelectedRows[0].Cells[1].Value = edit.InvName;
                dataGridView1.SelectedRows[0].Cells[2].Value = String.Format("{0:N2}", edit.Price);
                SaveState();
            }
        }

        private void btnDelete_Click(object sender, EventArgs e)
        {
            string message = "Are you sure you want to delete ";
            string message1 = dataGridView1.SelectedRows[0].Cells[1].Value.ToString();
            string message2 = dataGridView1.SelectedRows[0].Cells[2].Value.ToString();
            var result = MessageBox.Show(this, message + message1 + " (" + String.Format("{0:N2}", message2) + ")?", "Delete Item", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

            if (result == DialogResult.Yes)
            {
                dataGridView1.Rows.Remove(dataGridView1.SelectedRows[0]);
                dataGridView1.Refresh();
                SaveState();
            }
        }

        private void btnSell_Click(object sender, EventArgs e)
        {
            Sell sell = new Sell();
            Hide();
            sell.Text = "Sell Item " + dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
            sell.Check = int.Parse(dataGridView1.SelectedRows[0].Cells[3].Value.ToString());
            DialogResult answer = sell.ShowDialog();
            Show();

            double sales = 0;
            double testi = 0;
            testi += sales;

            if (answer == DialogResult.OK)
            {
                int test = 0;
                double price = 0;
                double grandTotal = 0;
                test = int.Parse(dataGridView1.SelectedRows[0].Cells[3].Value.ToString());
                test -= sell.Quant;
                dataGridView1.SelectedRows[0].Cells[3].Value = test;
                price = double.Parse(dataGridView1.SelectedRows[0].Cells[2].Value.ToString());
                sales = (sell.Quant * price);
                grandTotal = double.Parse(dataGridView1.SelectedRows[0].Cells[4].Value.ToString());
                grandTotal += sales;
                dataGridView1.SelectedRows[0].Cells[4].Value = String.Format("{0:N2}", grandTotal);
                SaveState();
            }
        }

        private void btnRestock_Click(object sender, EventArgs e)
        {
            Restock restock = new Restock();
            Hide();
            restock.Text = "Restock Item " + dataGridView1.SelectedRows[0].Cells[0].Value.ToString();
            DialogResult answer = restock.ShowDialog();
            Show();

            if (answer == DialogResult.OK)
            {
                int test = 0;
                test = int.Parse(dataGridView1.SelectedRows[0].Cells[3].Value.ToString());
                test += restock.Quant;
                dataGridView1.SelectedRows[0].Cells[3].Value = test;
                SaveState();
            }
        }

        private void btnExit_Click(object sender, EventArgs e)
        {
            string message = "Are you sure you want to exit?";
            string caption = "Exit";
            var result = MessageBox.Show(message, caption, MessageBoxButtons.YesNo, MessageBoxIcon.Question);

            if (result == DialogResult.Yes)
            {
                Application.Exit();
            }
        }
    }
}
