﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace FinalsProject
{
    public partial class Add : Form
    {

        private double rightPrice;
        private int rightStock;
        private int rightCode;

        public Add()
        {
            InitializeComponent();
        }

        public string Code
        {
            get
            {
                return rightCode.ToString();
            }
        }

        public string InvName
        {
            get
            {
                return txtName.Text;
            }
        }

        public double Price
        {
            get
            {
                return rightPrice;
            }
        }

        public int Stock
        {
            get
            {
                return rightStock;
            }
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            Form1 form = new Form1();

            if (txtCode.Text.Length == 5)
            {
                if (int.TryParse(txtCode.Text, out rightCode))
                {
                    if (int.Parse(txtCode.Text) > 0)
                    {
                        if (double.TryParse(txtPrice.Text, out rightPrice))
                        {
                            if (int.TryParse(txtStock.Text, out rightStock))
                            {
                                if (rightPrice >= 0)
                                {
                                    if (rightStock >= 0)
                                    {
                                        foreach (string codes in form.CodeList())
                                        {
                                            if (txtCode.Text == codes)
                                            {
                                                MessageBox.Show(this, "Invalid code. Product already exists.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                            }
                                            else
                                            {
                                                this.DialogResult = DialogResult.OK;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        MessageBox.Show(this, "Invalid stock. Must be positive.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    }
                                }
                                else
                                {
                                    MessageBox.Show(this, "Invalid price. Must be positive.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                }
                            }
                            else
                            {
                                MessageBox.Show(this, "Invalid stock. Not a number.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            }
                        }
                        else
                        {
                            MessageBox.Show(this, "Invalid price. Not a number.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        }
                    }
                    else
                    {
                        MessageBox.Show(this, "Invalid code. It must be 5 positive digits.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
                else
                {
                    MessageBox.Show(this, "Invalid inventory code. It must be 5 digits.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            else
            {
                MessageBox.Show(this, "Invalid inventory code. It must be 5 digits.", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void btnCancel_Click(object sender, EventArgs e)
        {
            this.DialogResult = DialogResult.Cancel;
        }
    }
}
